﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.Parser
{
    public class Token
    {
        public Token()
        {
            ID = -1;
            name = matchedValue = "";
        }
        public Token(int s, string n, string v)
        {
            ID = s;
            name = n;
            matchedValue = v;
        }
        public int ID;
        public string name;
        public string matchedValue;

        public void Copy(Token rhs)
        {
            ID = rhs.ID;
            name = rhs.name;
            matchedValue = rhs.matchedValue;
        }
        virtual public void Report()
        {
            Console.Write("[" + name + ":" + matchedValue + "]");
        }
    }
}
