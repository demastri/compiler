﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.Parser
{
    public class Sentence : Token
    {
        // it nominally works, let's refactor appropriately.
        // it's SLOW - keeping the two models (flat tokenID in template/instantiation here, and hierarchical ParseNode tree) is inefficient
        // the dynamic generation / extension / deletion of sentences is the issue - if we had one parse tree that we could walk, that might be better
        // Root is the target definition - one kid for each possible instantiation

        /// <summary>
        /// everything ends up as an instantiated token.
        /// A sentence definition is a series of 1+ tokens or sentences
        /// A sentence is an instance of a definition, ultimately with instantiated tokens that satisfy it
        ///     For each element in a sentence definition
        ///         if the element references a token, the instantiation is embedded
        ///         if the element references a sentence, the actually instantiated sentence definition is embedded
        ///         
        /// </summary>

        public Grammar.SentenceDef template;
        public List<Token> instantiation;
        public bool Active; // are we currently trying to instantiate this?
        public Sentence parentNode;   // if I'm a kid Sentence, here's where my parent is 
        public List<Sentence> deferralNode;   // if I'm instantiating a kid Sentence, here's where that will be managed
        public bool IsComplete; // is this sentence completely instantiated (for some path through it)
        public ParseNode node;

        public Sentence()
        {
            Init();
        }
        public Sentence(Grammar.SentenceDef sd)
        {
            Init();

            ID = sd.ID;
            template = sd;
            node = new ParseNode(sd.name, sd.regex, null);
            if (sd.tokenIDs.Count > 1)
                foreach (string s in sd.tokenIDs)
                    node.AddKid(new ParseNode(s, s, node));
        }
        public Sentence(Sentence rhs)
        {
            base.Copy(rhs);
            parentNode = rhs.parentNode;
            deferralNode = new List<Sentence>(rhs.deferralNode);
            template = new Grammar.SentenceDef(rhs.template);
            instantiation = new List<Token>(rhs.instantiation);
            ParseNode oldParent = (node == null ? null : node.parent);
            node = rhs.node.CloneTree();
            node.parent = oldParent;
        }
        public void Init()
        {
            ID = -1;
            template = null;
            parentNode = null;
            deferralNode = new List<Sentence>();
            instantiation = new List<Token>();
            Active = IsComplete = false;
            node = new ParseNode("", "", null);
        }

        public bool Consume(Token t)  // add this token to the current instantiation
        {
            // if this closes this definition, set IsComplete
            instantiation.Add(t);
            return IsComplete = (template.tokenIDs.Count == instantiation.Count);
        }

        // a parse tree would allow us to help with this.  
        // Each element (at every level in the tree) is either a token or a sentence responsible for a number of tokenIDs
        // initially, the sentence is responsible for 1 tokenID, as it's expanded it opens a set of kids
        public void ExpandForOption(int where, out Sentence possA, out Sentence possB)
        {
            int breakLoc, closeLoc;
            template.FindMatchingOptionDetails(where, out breakLoc, out closeLoc);

            // update the ParseTree
            ParseNode n;

            // now update the tokenIDs
            possA = new Sentence(this);   // remove curOpenSlot and from breakLoc -> closeLoc inclusive
            for (int l = closeLoc; l >= breakLoc; l--)
            {
                possA.template.tokenIDs.RemoveAt(l);
                n = possA.node.FindKidNode(l);
                n.parent.RemoveKid(n);
            }
            possA.template.tokenIDs.RemoveAt(where);
            n = possA.node.FindKidNode(where);
            n.parent.RemoveKid(n);
            possA.Active = true;

            possB = new Sentence(this);   // remove curOpenSlot -> breakLoc inclusive and closeLoc 
            possB.template.tokenIDs.RemoveAt(closeLoc);
            n = possB.node.FindKidNode(closeLoc);
            n.parent.RemoveKid(n);
            for (int l = breakLoc; l >= where; l--)
            {
                possB.template.tokenIDs.RemoveAt(l);
                n = possB.node.FindKidNode(l);
                if (!n.parent.kids.Contains(n))
                    continue;
                n.parent.RemoveKid(n);
            }
            possB.Active = true;

            Active = false;  // once we're live...
        }
        public void ExpandForSequence(int where, string tag, out Sentence possAddl)
        {
            possAddl = null;
            if (tag[tag.Length - 1] != '*')
                return;
            tag = tag.Substring(0, tag.Length - 1);
            possAddl = new Sentence(this);
            // update the ParseTree
            ParseNode n = possAddl.node.FindKidNode(where);
            int curLoc = n.parent.kids.IndexOf(n);
            n.parent.kids.Insert(curLoc, new ParseNode(tag, tag, n.parent));

            // now update the tokenIDs
            possAddl.template.tokenIDs.Insert(where, tag);
            possAddl.Active = true;
        }
        public Sentence Expand(int where, Grammar.SentenceDef expansion)
        {
            Sentence outCandidate = new Sentence(this);
            // update the ParseTree
            ParseNode n = outCandidate.node.FindKidNode(where);
            foreach( string s in expansion.tokenIDs )
                n.AddKid( new ParseNode( s, s, n ) );
            // now update the tokenIDs
            outCandidate.template.tokenIDs.RemoveAt(where);
            for (int ll = expansion.tokenIDs.Count - 1; ll >= 0; ll--)
                outCandidate.template.tokenIDs.Insert(where, expansion.tokenIDs[ll]);
            outCandidate.Active = true;

            Active = false;  // once we're live...
            return outCandidate;
        }

        public void Validate()
        {
            if (template.tokenIDs.Count != node.tokenSize)
                return;
            return;
        }

        public bool Expects(Token t)  // ### 1st draft ### does this token, by itself, satisfy the next slot
        {
            string tokenType = template.tokenIDs[instantiation.Count];
            if (t.name == tokenType)   // this is a match...
            {
                return true;
            }
            return false;
        }
        public bool CouldExpect(Token t)  // ### 1st draft ### could this token, by instantiating some sub-sentence, satisfy the next slot
        {
            return false;
        }
        public bool ConsumeAsSubSentence(Token t)  // ### 1st draft ### add this token to the current instantiation by opening the appropriate sub-sentence(s)?
        {
            // if this closes this definition, set IsComplete
            return false;
        }

        public bool IsDuplicate(Sentence s)
        {
            // if the instantiations and the token IDs match, they're identical
            if (s.template.tokenIDs.Count != template.tokenIDs.Count || s.instantiation.Count != instantiation.Count)
                return false;
            for (int i = 0; i < template.tokenIDs.Count; i++)
                if (s.template.tokenIDs[i] != template.tokenIDs[i])
                    return false;
            for (int i = 0; i < instantiation.Count; i++)
                if (((Token)(s.instantiation[i])).matchedValue != ((Token)(instantiation[i])).matchedValue)
                    return false;
            return true;
        }

        public string GetTokenMatch(ParseNode node, int offset)  // finds the one at that location...
        {
            int curOffset = 0;
            for( int x=0; x<offset; x++ )
                curOffset += node.kids[x].tokenSize;
            ParseNode working = node;
            while (working.parent != null)
            {
                int curIndex = working.parent.kids.IndexOf(working);
                for (int x = 0; x < curIndex; x++)
                    curOffset += working.parent.kids[x].tokenSize;
                working = working.parent;
            }
            return instantiation[curOffset].matchedValue;
        }
        public string GetTokenMatch(ParseNode node, string tokenID)  // finds the first one...
        {
            int curOffset = 0;
            if (node.kids != null)
            {
                foreach (ParseNode k in node.kids)
                {
                    if (k.name == tokenID)
                        break;
                    curOffset += k.tokenSize;
                }
            }
            ParseNode working = node;
            while (working.parent != null)
            {
                int curIndex = working.parent.kids.IndexOf(working);
                for (int x = 0; x < curIndex; x++)
                    curOffset += working.parent.kids[x].tokenSize;
                working = working.parent;
            }
            return instantiation[curOffset].matchedValue;
        }


        public string GetTokenIDs() // print tokenIDs for reference
        {
            string outString = "";
            foreach (string s in template.tokenIDs)
                outString += s + " ";
            outString += Environment.NewLine;
            return outString;
        }
        public string GetInstantiations()  // print instantiations for reference
        {
            string outString = "";
            foreach (Token s in instantiation)
                if (((Token)(s)).matchedValue != null)
                {
                    List<Grammar.TokenDef> tds = JPD.Parser.Grammar.CURRENTGRAMMAR.FindMatchingTokenDefs(((Token)(s)).name);
                    outString += tds[0].emit.Replace("%", ((Token)(s)).matchedValue);
                }
                else
                    outString += s.ToString();
            outString += Environment.NewLine;
            return outString;
        }
        override public void Report()
        {
            Console.WriteLine("xxxxxxxxxxxxxxxxxxxxx");

            Console.WriteLine(" Template: " + template.name + "  => " + template.regex);
            Console.WriteLine(" Active: " + (Active ? "Yes" : "No"));
            Console.WriteLine(" Complete: " + (IsComplete ? "Yes" : "No"));
            Console.Write(" ParseTree: ");
            node.Report();
            Console.WriteLine("");
            
            Console.Write(" TokenIDs: ");
            foreach (string s in template.tokenIDs)
                Console.Write(" "+s + ", " );
            Console.WriteLine("");
            Console.WriteLine(" Instantiations:");
            foreach (Token s in instantiation)
                s.Report();
            Console.WriteLine("");
            Console.WriteLine(" Deferral Nodes:");
            foreach (Sentence s in deferralNode)
                s.Report();
            Console.WriteLine("");
        }

        override public string ToString() // print tokenIDs/instantiated valued
        {
            string outString = template.name + " (" + template.description + "): " + Environment.NewLine;
            outString += GetTokenIDs();
            outString += GetInstantiations();
            return outString;
        }

    }
}
