﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.Parser
{
    class Operator : Sentence
    {
        public Operator()
        {
            lexicalReplace = "";
        }
        public Operator(string lr)
        {
            lexicalReplace = lr;
        }
        public string lexicalReplace;

        public string Transform(string source)
        {
            return source;
        }
    }
}
