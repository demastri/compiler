﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace JPD.Parser
{
    public class Parser
    {
        Grammar thisGrammar;
        public Parser()
        {
            thisGrammar = null;
        }
        public Parser(string grammarFile)
        {
            thisGrammar = new Grammar(grammarFile);
        }

        public List<Token> Tokenize(StreamReader corpusStream)
        {
            return Tokenize(thisGrammar.tokenDefs, corpusStream);
        }
        public List<Token> Tokenize(string corpusFile)
        {
            return Tokenize(thisGrammar.tokenDefs, new StreamReader(corpusFile));
        }
        public List<Token> Tokenize(string grammarFile, string corpusFile)
        {
            return Tokenize(new Grammar(grammarFile).tokenDefs, new StreamReader(corpusFile));
        }
        public List<Token> Tokenize(List<Grammar.TokenDef> tokenDefs, StreamReader inputCorpus)
        {
            List<Token> outTokens = new List<Token>();
            // match the input stream to the defined tokens in the grammar
            // if there's a break (could not tokenify part of the string), note the location, start at next line?

            while (!inputCorpus.EndOfStream)
            {
                string workingCorpus = inputCorpus.ReadLine();

                bool matched = false;
                while (workingCorpus.Length > 0)
                {
                    matched = false;
                    foreach (Grammar.TokenDef td in tokenDefs)
                    {
                        MatchCollection matches = Regex.Matches(workingCorpus, td.regex);
                        foreach (Match m in matches)
                        {
                            if (m.Index == 0)
                            {
                                outTokens.Add(new Token(td.ID, td.name, m.Value));
                                workingCorpus = workingCorpus.Substring(m.Length);
                                matched = true;
                                break;
                            }
                        }
                        if (matched)
                            break;
                    }
                }
            }
            return outTokens;
        }

        public List<Sentence> Compose(List<Token> tokens)
        {
            return Compose(thisGrammar, tokens);
        }
        public List<Sentence> Compose(Grammar grammar, List<Token> tokens)
        {
            List<Sentence> outSentences = new List<Sentence>();
            List<Sentence> candidates = null;

            bool resetContext = true;
            foreach (Token t in tokens)
            {
                if (resetContext)
                {
                    // actually reset the context
                    candidates = grammar.ResetContext();
                    resetContext = false;
                }
                for (int i = 0; i < candidates.Count; i++) // we may add to this during the loop, will clean up later
                {
                    bool consumedToken = false;

                    Sentence curCandidate = candidates[i];
                    if (!curCandidate.Active)
                        continue;
                    // ### implementation work to continue...
                    int curOpenSlot = curCandidate.instantiation.Count;
                    string curOpenSlotName = curCandidate.template.tokenIDs[curOpenSlot];
                    if (!consumedToken && grammar.IsOption(curOpenSlotName))
                    {
                        // this is ok - we're replacing an existing sentence with expanded ones based on embedded options
                        Sentence possA, possB;
                        curCandidate.ExpandForOption(curOpenSlot, out possA, out possB);

                        candidates.Add(possA);
                        candidates.Add(possB);

                        consumedToken = true;
                    }
                    if (!consumedToken && grammar.IsSequence(curOpenSlotName))
                    {
                        Sentence possSeq;
                        curCandidate.ExpandForSequence(curOpenSlot, curOpenSlotName, out possSeq);

                        candidates.Add(possSeq);
                    }
                    if (!consumedToken && grammar.IsToken(curOpenSlotName))
                    {
                        if (curOpenSlotName == t.name)  // If the slot matches the provided token, match, consume and continue
                        {
                            curCandidate.Consume(t);  // will also mark as complete if appropriate
                            consumedToken = true;
                        }
                        else    // no match - remove from live / mark as inactive and continue
                        {
                            curCandidate.Active = false;
                            if (curCandidate.parentNode != null)
                            {
                                curCandidate.parentNode.deferralNode.Remove(curCandidate);
                                curCandidate.parentNode = null;
                            }
                        }
                    }
                    if (!consumedToken && grammar.IsSentence(curOpenSlotName))
                    {
                        List<Grammar.SentenceDef> expansions = grammar.FindMatchingSentenceDefs(curOpenSlotName);
                        curCandidate.deferralNode = new List<Sentence>();
                        for (int l = 0; l < expansions.Count; l++)
                        {
                            Sentence exp = curCandidate.Expand(curOpenSlot, expansions[l]);
                            candidates.Add(exp);
                        }
                        curCandidate.Active = false;
                    }

                    // if after review, a sentence form is completely matched, accept it, add to the output and reset the context
                    while (curCandidate.Active && curCandidate.IsComplete)
                    {
                        if (curCandidate.parentNode != null)   // have to pop up one level 
                        {
                            Sentence parent = curCandidate.parentNode;
                            parent.instantiation.Add(curCandidate);
                            parent.deferralNode.Clear();        // ### this isn't right - the others might be the "right" one.
                            // ### need to expand lexically as originally implemented
                            // ### but we have to keep the logical sentence/token overlay
                            // ### for each expanded lexical token set
                            parent.Active = true;

                            curCandidate.Active = false;
                            curCandidate.parentNode = null;

                            curCandidate = parent;  // may or not be complete...
                        }
                        else
                        {
                            if (false)
                                curCandidate.Report();
                            bool isaDupe = false;
                            foreach (Sentence s in outSentences)
                                if (s.IsDuplicate(curCandidate))
                                    isaDupe = true;
                            if (!isaDupe)
                                outSentences.Add(curCandidate);
                            resetContext = true;
                            break;
                        }
                    }

                    if (false)
                    {
                        Console.WriteLine("Processing Token " + t.name + "/" + t.matchedValue);
                        grammar.Report();
                        foreach (Sentence s in candidates)
                            if (s.parentNode == null)
                                s.Report();
                    }

                }
                for (int i = 0; !resetContext && i < candidates.Count; i++) // clear dead sentences
                {
                    Sentence curContext = candidates[i];
                    if (!curContext.Active && (curContext.deferralNode == null || curContext.deferralNode.Count == 0))
                        candidates.RemoveAt(i--);  // clear this sentence (and don't skip the next one...)
                }
                for (int i = 0; i < candidates.Count; i++)
                    for (int j = i + 1; j < candidates.Count; j++)
                        if (candidates[i].IsDuplicate(candidates[j]))
                            candidates.RemoveAt(j--);


                if (false && candidates.Count == 0)
                {
                    Console.WriteLine("After Token " + t.name + "/" + t.matchedValue);
                    grammar.Report();
                    foreach (Sentence s in candidates)
                        if (s.parentNode == null)
                            s.Report();
                    //foreach (Sentence ss in candidates)
                    //    Console.WriteLine((ss.Active ? "Active " : "Inactive") + ss.GetTokenIDs() + ss.GetInstantiations());
                }

            }
            return outSentences;
        }
        public List<Sentence> Compose_FirstDraft(Grammar grammar, List<Token> tokens)
        {
            List<Grammar.SentenceDef> sentenceDefs = grammar.sentenceDefs;
            List<Sentence> outSentences = new List<Sentence>();
            List<Sentence> context = null;

            bool resetContext = true;
            foreach (Token t in tokens)
            {
                if (resetContext)
                {
                    // actually reset the context
                    context = grammar.ResetContext();
                    resetContext = false;
                }
                foreach (Sentence s in context)
                {
                    if (!s.Active)
                        continue;

                    bool consumedToken = false;
                    if (s.Expects(t))
                    {
                        // advance context so that the next expression is what we're waiting for now
                        s.Consume(t);
                        consumedToken = true;
                    }
                    if (s.CouldExpect(t))
                    {
                        // set context so that the sub-expressions are what we're waiting for now
                        s.ConsumeAsSubSentence(t);
                        consumedToken = true;
                    }
                    if (!consumedToken)
                    {
                        // ok - this sentence was not the one we were looking for...
                        s.Active = false;
                    }
                    if (s.IsComplete)
                    {
                        outSentences.Add(s);
                        resetContext = true;
                    }
                }
            }
            return outSentences;
        }
    }
}
