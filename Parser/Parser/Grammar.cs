﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace JPD.Parser
{
    public class Grammar
    {
        string name;
        string targetSentenceType;
        public static Grammar CURRENTGRAMMAR;        
        
        public List<TokenDef> tokenDefs;
        public class TokenDef
        {
            public int ID;
            public string name;
            public string regex;
            public string description;
            public string emit;

            public TokenDef(int s, string n, string r, string d)
            {
                ID = s;
                name = n;
                regex = r;
                description = d;
            }
            public TokenDef(XmlNode n)
            {
                ID = Convert.ToInt32(n.Attributes["ID"] != null ? n.Attributes["ID"].Value : "-1");
                name = n.Attributes["Name"] != null ? n.Attributes["Name"].Value : "";
                regex = n.Attributes["Regex"] != null ? n.Attributes["Regex"].Value : "";
                description = n.Attributes["Description"] != null ? n.Attributes["Description"].Value : "";
                emit = n.Attributes["Emit"] != null ? n.Attributes["Emit"].Value : "%";
            }
            public TokenDef(TokenDef rhs)
            {
                ID = rhs.ID;
                name = rhs.name;
                regex = rhs.regex;
                description = rhs.description;
            }
        }
        public List<SentenceDef> sentenceDefs;
        public class SentenceDef : TokenDef
        {
            public List<string> tokenIDs;
            
            public SentenceDef(XmlNode n, List<TokenDef> tds, List<SentenceDef> sds)
                : base(n)
            {
                regex = n.Attributes["Pattern"] != null ? n.Attributes["Pattern"].Value : "";

                InitTokenIDs(tds, sds);
            }
            public SentenceDef(int s, string n, string r, string d, string p, List<TokenDef> tds, List<SentenceDef> sds)
                : base(s, n, r, d)
            {
                tokenIDs = new List<string>();
                InitTokenIDs(tds, sds);
            }
            public SentenceDef(SentenceDef rhs) : base(rhs)
            {
                tokenIDs = new List<string>(rhs.tokenIDs);
            }
            private void InitTokenIDs(List<TokenDef> tds, List<SentenceDef> sds)
            {
                tokenIDs = new List<string>();
                string thisPattern = regex;
                while (thisPattern.Length > 0)
                {
                    int lastLength = thisPattern.Length;

                    if (thisPattern[0] == '[')
                    {
                        tokenIDs.Add("##OptionOpen##");
                        thisPattern = thisPattern.Substring(1);
                    }
                    if (thisPattern[0] == '|')
                    {
                        tokenIDs.Add("##OptionBreak##");
                        thisPattern = thisPattern.Substring(1);
                    }
                    if (thisPattern[0] == ']')
                    {
                        tokenIDs.Add("##OptionClose##");
                        thisPattern = thisPattern.Substring(1);
                    }

                    foreach (TokenDef td in tds)
                        if (thisPattern.IndexOf(td.name) == 0)
                        {
                            tokenIDs.Add(td.name);
                            thisPattern = thisPattern.Substring(td.name.Length);
                            break;
                        }
                    foreach (SentenceDef sd in sds)
                        if (thisPattern.IndexOf(sd.name) == 0)
                        {
                            tokenIDs.Add(sd.name);
                            thisPattern = thisPattern.Substring(sd.name.Length);
                            break;
                        }
                    if (thisPattern.Length > 0 && thisPattern[0] == '*')   // sequence of these things!
                    {
                        tokenIDs[tokenIDs.Count - 1] = tokenIDs[tokenIDs.Count - 1] + "*";
                        thisPattern = thisPattern.Substring(1);
                    }
                    if (lastLength == thisPattern.Length)
                    {
                        Console.WriteLine("Couldn't match pattern for <" + thisPattern + ">");
                        break;
                    }
                }
            }
            public void FindMatchingOptionDetails(int start, out int mid, out int end)
            {
                end = mid = -1;
                int curOpenCount = 0;
                if (tokenIDs[start] != "##OptionOpen##")
                    return;
                for (int i = start+1; i < tokenIDs.Count; i++)
                {
                    if (tokenIDs[i] == "##OptionBreak##" && curOpenCount == 0)
                        if (curOpenCount == 0)
                            mid = i;
                    if (tokenIDs[i] == "##OptionOpen##")
                        curOpenCount++;
                    if (tokenIDs[i] == "##OptionClose##")
                        if (curOpenCount == 0)
                            end = i;
                        else
                            curOpenCount--;
                }
            }
        }
        public Grammar(string fileLoc)
        {
            /// a grammar needs to define two things:
            /// 1 - the lexical definition of the tokens it recognizes
            /// 2 - the logical relationship between the tokens that define this grammar
            /// The example Grammar for initial implementation will be RPN arithmetic notation for 4 basic ops, paren and unary +-
            /// The testing Grammar will be PGN notation
            XmlDocument grammarDoc = new XmlDocument();
            grammarDoc.Load(fileLoc);

            XmlNode rootNode = grammarDoc.SelectSingleNode("Grammar");
            name = rootNode.Attributes["Name"] != null ? rootNode.Attributes["Name"].Value : "";
            targetSentenceType = rootNode.Attributes["Target"] != null ? rootNode.Attributes["Target"].Value : "";

            tokenDefs = new List<TokenDef>();
            foreach (XmlNode node in grammarDoc.SelectNodes("Grammar/Tokens/Token"))
            {
                tokenDefs.Add(new TokenDef(node));
            }

            sentenceDefs = new List<SentenceDef>();
            foreach (XmlNode node in grammarDoc.SelectNodes("Grammar/Sentences/Sentence"))
            {
                sentenceDefs.Add(new SentenceDef(node, tokenDefs, sentenceDefs));
            }
            CURRENTGRAMMAR = this;
        }

        public List<Sentence> ResetContext()
        {
            List<Sentence> outList = new List<Sentence>();

            foreach (SentenceDef sd in sentenceDefs)
            {
                if (sd.name == targetSentenceType)
                {
                    Sentence possible = new Sentence(sd);
                    possible.Active = true;
                    outList.Add(possible);
                }
            }
            return outList;
        }
        public bool IsOption( string tag )
        {
            return tag == "##OptionOpen##";
        }
        public bool IsSequence(string tag)
        {
            return tag[tag.Length - 1] == '*';
        }
        public string GetSingleName(string tag)
        {
            if (!IsSequence(tag))
                return tag;
            return tag.Substring(0, tag.Length - 1);
        }
        
        public bool IsToken( string tag )
        {
            tag = GetSingleName(tag);
            foreach (TokenDef td in tokenDefs)
                if (td.name == tag)
                    return true;
            return false;
        }
        public bool IsSentence( string tag )
        {
            tag = GetSingleName(tag);
            foreach (SentenceDef sd in sentenceDefs)
                if (sd.name == tag)
                    return true;
            return false;
        }
        public List<SentenceDef> FindMatchingSentenceDefs(string tag)
        {
            tag = GetSingleName(tag);
            List<SentenceDef> outList = new List<SentenceDef>();
            foreach (SentenceDef sd in sentenceDefs)
                if (sd.name == tag)
                    outList.Add(sd);
            return outList;
        }
        public List<TokenDef> FindMatchingTokenDefs(string tag)
        {
            tag = GetSingleName(tag);
            List<TokenDef> outList = new List<TokenDef>();
            foreach (TokenDef sd in tokenDefs)
                if (sd.name == tag)
                    outList.Add(sd);
            return outList;
        }
        public void Report()
        {
            Console.WriteLine("=====================================");
            Console.WriteLine("Token Definitions:");
            foreach (TokenDef td in tokenDefs)
                Console.WriteLine(" " + td.name + "  =>  " + td.regex);
            Console.WriteLine("");
            Console.WriteLine("Sentence Definitions:");
            foreach (SentenceDef sd in sentenceDefs)
            {
                Console.Write(" " + sd.name + "  =>  ");
                foreach (string s in sd.tokenIDs)
                    Console.Write(s + ", ");
                Console.WriteLine("");
            }
            Console.WriteLine("");
        }
    }
}
