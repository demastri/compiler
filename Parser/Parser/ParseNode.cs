﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JPD.Parser
{
    public class ParseNode
    {
        public string name;
        public string implPattern;
        public List<ParseNode> kids;
        public ParseNode parent;
        private int staticTokenSize;
        public int tokenSize
        {
            get
            {
                return staticTokenSize;
                if (kids == null || kids.Count == 0) return 1;
                int outVal = 0; foreach (ParseNode k in kids) outVal += k.tokenSize; return outVal;
            }
        }

        public ParseNode(string n, string p, ParseNode pn)
        {
            name = n;
            implPattern = p;
            kids = null;
            parent = pn;
            staticTokenSize = 1;
        }

        public void AddKid(ParseNode k)
        {
            if (kids == null || kids.Count == 0)
                staticTokenSize = 0;
            if (kids == null)
                kids = new List<ParseNode>();
            kids.Add(k);
            staticTokenSize += k.tokenSize;
            ParseNode curParent = parent;
            while (curParent != null)
            {
                curParent.staticTokenSize++;
                curParent = curParent.parent;
            }

        }
        public void RemoveKid(ParseNode k)
        {
            staticTokenSize -= k.tokenSize;
            if (staticTokenSize == 0)
                staticTokenSize = 1;
            kids.Remove(k);
        }
        public ParseNode FindKidNode(int where)
        {
            int curLoc = 0;
            if (where == 0 && (kids == null || kids.Count == 0))
                return this;
            if (kids != null)
                foreach (ParseNode kid in kids)
                {
                    if (curLoc + kid.tokenSize <= where) // not this one...
                        curLoc += kid.tokenSize;
                    else // >=, so it's this one...
                    {
                        if (curLoc == where && kid.kids != null && kid.kids.Count == 0)
                            return kid;
                        return kid.FindKidNode(where - curLoc);
                    }
                }
            return null;
        }
        public ParseNode CloneTree()
        {
            ParseNode outNode = new ParseNode(name, implPattern, null);
            if (kids != null)
                foreach (ParseNode nn in kids)
                {
                    ParseNode next = nn.CloneTree();
                    outNode.AddKid(next);
                    next.parent = outNode;
                }
            return outNode;
        }
        public void Report()
        {
            Console.Write(" " + implPattern);
            if (kids != null && kids.Count > 0)
            {
                Console.Write(" {{ ");
                foreach (ParseNode kid in kids)
                    kid.Report();
                Console.Write(" }} ");
            }
        }
        public List<ParseNode> FindKids(string search)
        {
            List<ParseNode> poss = new List<ParseNode>();
            poss.Add(this);
            string[] kids = search.Split('/');
            foreach (string level in kids)
            {
                string compRef = level;
                if (level.Length >1 && level[level.Length - 1] == '*')
                    compRef = level.Substring(0, level.Length - 1);
                List<ParseNode> nextLevel = new List<ParseNode>();
                foreach (ParseNode curParent in poss)
                    foreach (ParseNode k in curParent.kids)
                    {
                        string compVal = k.name;
                        if (compVal[compVal.Length - 1] == '*')
                            compVal = compVal.Substring(0, compVal.Length - 1);
                        if (compVal == compRef || compRef == "*")
                            nextLevel.Add(k);
                    }
                poss = nextLevel;
            }
            return poss;
        }
    }
}
