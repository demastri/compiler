﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using JPD.Parser;
using JPD.Compiler;

namespace JPD.Compiler.Driver
{
    class Program
    {
        static void Main(string[] args)
        {
            string grammarFile = "Parser/Grammars/RPN.xml";
            string corpusFile = "Parser/Corpora/RPN Sample.txt";

            grammarFile = "Parser/Grammars/PGNSchema.xml";
            corpusFile = "Parser/Corpora/Sample.pgn";
            corpusFile = "C:\\Users\\jdemast\\OneDrive\\Documents\\chess\\Live EK Games.pgn";

            Parser.Parser p = new Parser.Parser(grammarFile);
            List<Parser.Token> tokens = p.Tokenize(corpusFile);
            List<Parser.Sentence> sentences = p.Compose(tokens);

            foreach (Sentence s in sentences)
            {
                Console.WriteLine(s.ToString());
                //s.Report();
            }
        }
    }
}
